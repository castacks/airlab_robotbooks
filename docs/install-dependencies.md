# Azure Tools Install

*If you already have all these tools installed, feel free to skip this tutorial.*

On your localhost, install the following thirdparty tools.

## Ansible

        # Add ansible to install
        sudo apt update
        sudo apt install software-properties-common
        sudo apt-add-repository --yes --update ppa:ansible/ansible

        # Install ansible
        sudo apt install ansible
