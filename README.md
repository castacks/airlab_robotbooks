# Robotbooks: Ansible

[TOC]

There is one operational tools available to use: `ansible`

- Ansible is a set of scripts that automate installing packages & configurations for different systems.

- Feel free to update any of the ansible scripts if there are missing dependencies.

The `ansible` scripts do not give realtime output.

- You will only see the success of the commands after it has completed.
- Some commands might take a long time to complete, so wait until `ansible` returns a fail or pass status.

**Things to be keep in mind:**

- If a command takes longer than 30 minutes, check your VM or VPN connection.
- If you see an error status for any task, please notify the maintainer.
- The `ansible` scripts will connect to the remote VMs and run the installs. Verify you have `ssh` access to the VMs.
- You can run the ansible install scripts in parallel for different hosts.
- **Ansible script should only fail if there has been a network disconnection. If there are any other failures, please notify the maintainer.**

# Getting Started

The below instructions should get you started on a basic `ansible` setup.

- Please make sure you have completed the [Azure tutorials](https://bitbucket.org/castacks/azurebooks/src/master/) before continuing with ansible below.

You will need to go through a few tutorials to have a working system.

## 1. Install Third-Party Tool (Required)

**Instructions at:**

- [`docs/install-dependencies.md`](docs/install-dependencies.md)

This tutorial will setup the following:

- installs thirdparty tools such as `ansible` etc., required to setup ansible infrastructure.

## 2. Azure Virtual Machine Prerequisites (Required)

**Select your project's relevant instructions:**

- Airlab Tutorial Week: [`airlab-tutorial/docs/azure-prereq.md`](airlab-tutorial/docs/azure-prereq.md)

This tutorial will setup the following:

- Setup prerequisites needed for using your project's Azure VMs.

## 3. Prepare The Azure Virtual Machines (Required)

**Select your project's relevant instructions:**

- Airlab Tutorial Week: [`airlab-tutorial/docs/azure-prepare.md`](airlab-tutorial/docs/azure-prepare.md)

This tutorial will setup the following:

- Install all the linux system package dependencies on the Azure VMs.
- If there are any dependencies missing, please notify the maintainer.

