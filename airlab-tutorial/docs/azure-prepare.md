# Build With Azure Prepare

## About

You will need to prepare the Azure VMs by installing all the dependencies you have already setup on your localhost and more.

- There are `ansible` scripts available that automates this process.

The `ansible` scripts can be found at: `operations/deploy/robotbooks`

- Users can add to the scripts if there are missing dependencies.

The `ansible` scripts do not give realtime output.

- You will only see the success of the commands after it has completed.
- Some commands might take a long time to complete (as long as 20 minutes for cloning all the submodules), so wait until `ansible` returns a fail or pass status.

**Things to be keep in mind:**

- If a command takes longer than 30 minutes, check your VM or VPN connection.
- If you see an error status for any task, please notify the maintainer.
- The `ansible` scripts will connect to the remote VMs and run the installs. Verify you have `ssh` access to the VMs.
- You can run the `basestation`, `ugv`, `uav` install steps in parallel of each other (meaning steps 3, 4, 5 can be run in parallel of each other).

**Please, run the following instructions from your localhost.**


## 1. Verify Localhost Setup: Bitbucket SSH Keys

        # verify you have the bitbucket keys available (Verify on bitbucket website that you have setup this ssh key)
        ls ~/.ssh/bitbucket
        ls ~/.ssh/bitbucket.pub

## 2. Verify Azure VM Connection

        # run a custom infrastructure script, which checks all your ssh connections listed in ~/.ssh/config
        # -- you can tab-complete 'infra' to see what other infrastructure tools are available.
        infra-check-ssh

## 3. Ansible Workspace

        # go to the ansible workspace
        cd ~/infrastructure_ws/src/robotbooks/airlab-tutorial

        # verify the available hosts that are setup
        ansible-playbook -v -i inventory/azure.ini install-azure.yaml --list-hosts

## 4. Install CPU VM Dependencies

        # Verify VM Access
        ping -c 3 azure-tutorial-cpu

        # == CPU VM Install ==
        # Install basic dependencies on the remote VM (system, docker, docker tools)
        # Clones the infrastructure repo on the remote VM
        # You do not need to clone the repo on the remote VM manually, this command will do that for you.
        ansible-playbook -v -i inventory/azure.ini install-azure.yaml --limit azure-tutorial-cpu

## 5. Install GPU VM Dependencies

        # Verify VM Access
        ping -c 3 azure-tutorial-gpu

        # == GPU VM Install ==
        # Install basic dependencies on the remote VM (system, docker, docker tools)
        # Clones the infrastructure repo on the remote VM
        # You do not need to clone the repo on the remote VM manually, this command will do that for you.
        ansible-playbook -v -i inventory/azure.ini install-azure.yaml --limit azure-tutorial-gpu

## 6. Verify Install

Verify everything was installed correctly on all the VMs.

Example steps below show how to verify on the basestation VM:

        # access the remote VM
        ssh azure.tutorial.cpu

        # verify the deploy workspaces exists
        cd ~/infrastructure_ws/src

        # verify git status shows: "nothing to commit, working tree clean" and are on the correct branch
        git status

        # verify the repos exist:
        ls -all .

        # verify all the tools

        # verify docker
        docker --version

        # verify docker-compose
        docker-compose -v

        # verify docker-compose shows the help usage message
        docker-compose-wrapper --help

        # verify deployer script shows the help usage message
        ./deployer --help

        # exit the azure CPU VM
        exit

        # check teamviewer is available on VMs -- use teamviewer IDs to login to the VM
        # - default teamviewer password is `teamveiwer`
        # - default VM username is `airlab`
        # - default VM password is `Password1234!`
        infra-check-teamviewer
