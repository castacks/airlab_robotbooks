# Azure Virtual Machine Prerequisites

The Azure instructions assumes that the user has already completed your project *Azure Cloud Infrastructure Setup*.

Please follow the below instructions **on your localhost** (not on the VMs).

If you VM configures a dynamic IP, you will need to keep the `/etc/hosts` file updated. Or change the terraform files to use a static IP instead.

## 1. Find VM Private IP

- Go to the Azure Portal Website

- Or, run the command below:

        az vm list-ip-addresses -g airlab -o table | grep [your azure username]

## 2. Setup Remote Host Alias

        # open local host file
        sudo vim /etc/hosts

        # Add the remote vm hosts.
        # The below is configured to match the Azure setup tutorial in azurebooks/airlab, so please use these configurations.
        # If you have a different Azure VM setup, then you will need to change these values.

        VM-CPU-Private-IP-GOES HERE        azure-tutorial-cpu
        VM-GPU-Private-IP-GOES HERE        azure-tutorial-gpu

**Verify Azure VM Ping Access**

You should be able now to ping the remote host using the above alias.

        # ping the azure azure-tutorial-cpu
        ping azure-tutorial-cpu

        # ping the azure azure-tutorial-gpu
        ping azure-tutorial-gpu

## 3. Setup Remote Host SSH Config

Please setup the ssh config for all available Azure VMs.

        # open local ssh config file
        vim ~/.ssh/config

        # == Add the following below to the `~/.ssh/config` file (dont add the comments )==

        # enable rsa keys to use on login
        IdentityFile ~/.ssh/bitbucket

        # Add all the remote vm ssh configuration.
        # The below is configured to match the Azure setup tutorial in azurebooks/airlab, so please use these template configurations.
        # About the configuration template:
        #       - The HostName must match what was configured in /etc/hosts
        #       - The 'IdentityFile' is the path to the ssh key used to access the Azure VM (the azure tutorial sets key as the path ~/.ssh/azure_vpn)
        #       - The default user for all the Azure VM is `airlab`       (the azure tutorial sets the username as `airlab`)

        Host azure.tutorial.cpu
          HostName azure-tutorial-cpu
          User airlab
          IdentitiesOnly yes
          IdentityFile ~/.ssh/azure/vpn/tutorial/vm_key

        Host azure.tutorial.gpu
          HostName azure-tutorial-gpu
          User airlab
          IdentitiesOnly yes
          IdentityFile ~/.ssh/azure/vpn/tutorial/vm_key

## 4. Verify Azure VM SSH Access

You should be able now to ssh into the remote host using the above alias.

        # ssh into the azure cpu vm
        ssh azure.tutorial.cpu

        # ssh into the azure gpu vm
        ssh azure.tutorial.gpu
